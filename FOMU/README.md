FOMU Instructions:

1. FOMU Setup and Installation
    a. Linux
    b. Windows

2. Run and Compilation
    a. non-blinky hello world
    b. ro-fomu example {our ro modules}

## FOMU Setup and Installation
## Linux
To set up FOMU in linux follow the following instructions:
a. Download this current repository which contains nb-fomu-hw and ro-fomu-hw.
nb-fomu-hw is a basic example that ouputs hello world to the serial port.
ro-fomu-hw is our ring oscillator design.
It should be noted that you still have to clone the nb-fomu-hw repo even after downloading this repo. This is done by ` git clone https://github.com/stef/nb-fomu-hw.git`
In addition, you also need to clone the  tinyfpga_bx_usbserial repo. This is done by `git clone https://github.com/davidthings/tinyfpga_bx_usbserial.git`

b. Ensure you have the latest version of  dfu-util running. To check your current version, type in `dfu-util -l`
If you don't have install it using `sudo apt install dfu-util`

c. The FOMU tool chain is needed and contains dependencies needed by  for it's programming.
Download the current version from `https://github.com/im-tomu/fomu-toolchain/releases` and unzip the file.
Make sure you download the `.tar.gz file`.
The tar file can be unzipped using `tar -xvf <Name of tar file>`

d. The FOMU toolchain directory has to be added to the PATH.
This is done using `export PATH=[path-to-toolchain]/bin:$PATH`
In addition, Examples in VHDL on Fomu and Mixed HDL on Fomu use ghdl-yosys-plugin, which looks for standard libraries in the path used when GHDL was configured/built. Therefore, when the toolchain is extracted to an arbitrary location, GHDL_PREFIX needs to be set. This is done using `export GHDL_PREFIX=[path-to-toolchain]/lib/ghdl`
These export commands can be added at the end of .bashrc to make it more streamlined.

e. FOMU toolchain uses yosys. To ensure that it is installed correctly run `yosys`.
The output should be similar to this:

```
/----------------------------------------------------------------------------\
 |                                                                            |
 |  yosys -- Yosys Open SYnthesis Suite                                       |
 |                                                                            |
 |  Copyright (C) 2012 - 2020  Claire Wolf <claire@symbioticeda.com>          |
 |                                                                            |
 |  Permission to use, copy, modify, and/or distribute this software for any  |
 |  purpose with or without fee is hereby granted, provided that the above    |
 |  copyright notice and this permission notice appear in all copies.         |
 |                                                                            |
 |  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  |
 |  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF          |
 |  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR   |
 |  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    |
 |  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN     |
 |  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF   |
 |  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.            |
 |                                                                            |
 \----------------------------------------------------------------------------/

 Yosys 0.9+3619 (open-tool-forge build) (git sha1 c403c984, gcc 9.3.0-17ubuntu1~20.04 -Os)


yosys> 

Type `exit` to then quit yosys
```

f. The hardware needed is the FOMU Production (pvt) version. Ensure that it is connected proprely using the USB Cable.

g. udev rules need to be setup to grant permission for using the USB device from a non-privileged account.
On GNU/Linux, try running `dfu-util -l`. If you get an error message like the following, you should add a udev rule as to give your user permission to the USB device.
dfu-util: Cannot open DFU device 1209:5bf0

To setup udev, add your user to group plugdev.
This is done by `sudo groupadd plugdev` and `sudo usermod -a -G plugdev $USER`

Use id $USER and/or groups to check you are in group plugdev:
This is done by `id $USER` and `groups | grep plugdev`

Create a file named /etc/udev/rules.d/99-fomu.rules and add the following:
`SUBSYSTEM=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="5bf0", MODE="0664", GROUP="plugdev" `

Reload the udev-rules using the following:
`sudo udevadm control --reload-rules`
`sudo udevadm trigger`

h. The Serial port `/dev/ttyACMO` might bot be present when the FOMU is plugged in. The reason for this seems to be unclear.




i. The FOMU Bootloader may need to updated to the latest version.
To update your Fomu, download the appropriate -updater dfu release from `https://github.com/im-tomu/foboot/releases/latest`.

Download the board  pvt-updater-[version-number].dfu file.

Run dfu-util -D pvt-[version-number].dfu. For example ``dfu-util -D ~/Downloads/pvt-updater-v2.0.4.dfu`

The Fomu will flash rainbow for about five seconds, then reboot and go back to blinking.

To verify it has updated, run `dfu-util -l` and check the version output.


### Enabling the Serial Port
The FOMU uses the serial port `/dev/tty/ACM0`. You could use `screen /dev/tty/ACM0` or simply minicom and select the serial port.



## Windows

For the `fomu-toolchain-linux` download WSL2 by following instructions given at https://docs.microsoft.com/en-us/windows/wsl/install

After the installation of WSL follow all the steps from FOMU Setup installation and Linux (step a - step g)



### Required Drivers

*Note: This set up is only needed for Windows system **earlier** than Windows 10.*

For installation follow the steps

1. Download and install Zadig (https://zadig.akeo.ie/)

2. Open Zadig

3. Under `Options`, select `List All Devices`

4. In the dropdown, select your Fomu; in the field right of the green arrow, choose the `WinUSB` driver; and hit `Upgrade Driver`.

   

### Setting up dfu-util 

Then set the path to dfu-util for windows by running following command

​	$ `export PATH=[path-to-toolchain]/dfu-util-win/bin-win64:$PATH`

#### Sanity check for dfu-util

Run following command 

`$dfu-util.exe -l`

This should output something similar to following (if the fomu is connected):

```
dfu-util 0.9

Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
Copyright 2010-2016 Tormod Volden and Stefan Schmidt
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Cannot open DFU device 04f2:b6cb
Found DFU: [1209:5bf0] ver=0101, devnum=29, cfg=1, intf=0, path="2-1.4", alt=0, name="Fomu PVT running DFU Bootloader v1.9.1", serial="UNKNOWN"
```





### Enabling the Serial Port

- ​	Download Tera Term (https://ttssh2.osdn.jp/index.html.en)  (or) Putty (https://www.putty.org/) (or) any other preferable software on which serial communication can be observed. 
- ​	The FOMU sends serial data on one of the COM ports once the FOMU is programmed specific COM port will be enabled. Select the COM port on one of the selected aforementioned softwares
  - For e.g., open Tera Term -> open new connection -> select serial -> from 'port' dropdown select the correct COM port






## Run and Compilation

## nb-fomu-hw
The nb-fomu-hw example contains two examples.
## 1.  nb_fomu_hw
This prints helloworld onto the USB serial port by connecting to `/dev/ttyACM0`

#### Linux

- To test this on the FOMU run `make hw-load`. This compiles the example and loads it into the FOMU FPGA. 
- Then run `screen /dev/ttyACM0`

#### Windows

- To test this on the FOMU run `make hw-load-win`. This compiles the example and loads it into the FOMU FPGA. 
- Then use serial monitor such as teraterm or Putty to observe the serial output.



## 2. fomu_led_ctrl
This allows you to control the colors of the LED by sending the characters `r`,`g` or `b` to the serial port. Any other character shuts off the LED.

#### Linux

- To test this on the FOMU run `make ctrl-load`. This compiles the example and loads it into the FOMU FPGA. 
- Then run `screen /dev/ttyACM0`

#### Windows

- To test this on the FOMU run `make ctrl-load-win.`
- Then use serial monitor such as teraterm or Putty to observe the serial output.



## ro-fomu-hw
This is our own ring oscillator design.
To make the ro design cd into the ro-fomu-hw directory and run `make ro-load`

