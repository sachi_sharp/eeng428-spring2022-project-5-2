
/*
 *  
 * Copyright (C) 2022
 * Author: Anthony Etim <anthony.etim@yale.edu>
 *          
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
*/


`include "ro_defines.sv"

module ring_oscillator #(
    parameter RO_TYPE   = `RO_LUT,
    parameter STAGES    = 2
)(
    input               enable,
    output              ro_out
);

// Added additional stage as otherwise too fast for counting (timing violations)

localparam LAST         = STAGES+1;

(* keep="true" *) wire [LAST:0]           ro_wire;

assign ro_out = ro_wire[LAST];

// LUT2
(* keep="true" *)
SB_LUT4 #(
    .LUT_INIT           (16'd4)
) ro_inv (
    .I0                 (ro_wire[0]),
    .I1                 (enable),
    .I2                 (1'b0),
    .I3                 (1'b0),
    .O                  (ro_wire[1])
);

genvar i;
generate

// LUT1 
for (i = 0; i < STAGES; i = i + 1) begin : ro_stages
    (* keep="true" *)
    SB_LUT4  #(
        .LUT_INIT       (16'd2)
    ) ro_buf (
        .I0             (ro_wire[i + 1]),
        .I1             (1'b0),
        .I2             (1'b0),
        .I3             (1'b0),
        .O              (ro_wire[i + 2])
    );
end

if (RO_TYPE == `RO_LUT) begin

if (STAGES < 0) begin
assign ro_wire[0] = ro_wire[LAST];
end else begin
(* keep="true" *)
SB_LUT4 #(
   .LUT_INIT            (16'd2)
) ro_lut (
    .I0                 (ro_wire[LAST]),
    .I1                 (1'b0),
    .I2                 (1'b0),
    .I3                 (1'b0),
    .O                  (ro_wire[0])
);
end

end else if (RO_TYPE == `RO_REGISTER) begin

(* keep="true" *) wire ff_clk;

(* keep="true" *)
SB_LUT4 #(
   .LUT_INIT            (16'd1)
) ro_clk_lut (
    .I0                 (ro_wire[LAST]),
    .I1                 (1'b0),
    .I2                 (1'b0),
    .I3                 (1'b0),
    .O                  (ff_clk)
);

(* keep="true" *)
SB_DFFES ro_ff (
    .S                  (ro_wire[LAST]),
    .E                  (1),
    .D                  (0),
    .C                  (ff_clk),
    .Q                  (ro_wire[0])
);

end else begin
initial begin
    $error("Invalid RO_TYPE. Allowed values: RO_LUT (%d), RO_REGISTER (%d)", RO_LUT, RO_REGISTER);
end
end

endgenerate

endmodule
