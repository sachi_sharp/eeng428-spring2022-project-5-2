// top-level testbench module for a synchronous FIFO

module fifo_tb;

// main simulated clock
reg [0:0] clock;

// temporary counter regster for testing FIFO writes
reg [FIFO_LGFLEN:0] counter = 0;

// parameter for the FIFO, used to specify the data width
// need to define early so that it can be used 
// to specify input register and output wire widhts as well
localparam DATA_WIDTH = 16;

// parameter for the FIFO, the depth of the FIFO, slecified as log base 2
// of the FIFO depth
localparam FIFO_LGFLEN = 12;

// port connections for the design under test, DUT,
// i.e. the FIFO that is being tested

// inputs to the DUT should be registers
reg wr;
reg [DATA_WIDTH-1:0] din;
reg rd;

// outputs from the DUT should be wires
wire full;
wire [FIFO_LGFLEN:0] fill;
wire [DATA_WIDTH-1:0] dout;
wire empty;

// the FIFO
fifo #(
    .BW(DATA_WIDTH),       // data width
    .LGFLEN(FIFO_LGFLEN)   // log of the FIFO length, 2 --> 4 entry FIFO
) fifo_inst (
    .i_clk(clock),
    .i_wr(wr),
    .i_data(din),
    .o_full(full),
    .o_fill(fill),
    .i_rd(rd),
    .o_data(dout),
    .o_empty(empty)
);

// monitor all the signals
initial
begin
  $monitor ($time,,,"clock=%b wr=%b din=%b full=%b fill=%b rd=%b dout=%b empty=%b",
             clock, wr, din, full, fill, rd, dout, empty);
  #500 $finish;
end

// dump signal traces into a VCD file
initial
begin
  $dumpfile("fifo-tb-waveform.vcd");
  $dumpvars(0,fifo_tb);
end

// initialize clock at start of simulation
initial
begin
  #0 clock = 0;
end

// every 5 cycles of simulation, toggle the clock
always
begin
  #5 clock = ~ clock;
end

// main testbech inputs here
initial
begin

  // write a piece of data
  #10;
  wr = 1;
  din = 8'd10;
  #10;
  wr = 0;
  din = 8'd0;

  // read a piece of data
  #10;
  rd = 1;
  #10;
  rd = 0;
  
  // write more data
  while(!full)
  begin
    #10;
    wr = 1;
    din = counter;
    counter = counter + 1;
  end

  #10;
  wr = 0;

  // more testing can be added here...

end

endmodule