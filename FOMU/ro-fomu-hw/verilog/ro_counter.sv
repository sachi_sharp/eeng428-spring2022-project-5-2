`include "ro_defines.sv"

module ro_counter #(
    parameter COUNTER_BIT    = 32,
    parameter RO_TYPE        = `RO_LUT,
    parameter CLOCKS_PER_BIT = 4_194_304,
	parameter STAGES         = 2
)  (
    input wire clk_48mhz,
    input wire reset,
    input wire debug_syncup,
    output wire [31:0] ro_comp_count
);

////////////////////////////
// RO sensor
////////////////////////////
// ro ticks
 wire ro_out;
// enable signal for ro
reg ro_enable_internal = 1'b1;

(* keep="true" *) 
ring_oscillator #(
    .RO_TYPE(`RO_TYPE),
    .STAGES(`STAGES)
) ro_INST(
    .enable(ro_enable_internal),
    .ro_out(ro_out)
);

////////////////////////////
// local paramters or reg generated from parameters
////////////////////////////

(* keep="true" *) reg         ro_counter_reset = 0;
(* keep="true" *) wire [31:0] ro_counter_count;

(* keep="true" *) 
counter #(
    .COUNTER_BIT(COUNTER_BIT)
) counter_RO(
    .tick(ro_out),
    .reset(ro_counter_reset),
    .count(ro_counter_count)
);

////////////////////////////
// RO COMPARATOR WIRES
////////////////////////////
reg [31:0] ro_comp_count_reg;
assign ro_comp_count = ro_comp_count_reg;

////////////////////////////
// State machine parameters
////////////////////////////

localparam IDLE             = 3'd0;
localparam ROMEASURE        = 3'd1;
localparam CONTINUE_MEASURE = 3'd2;
////////////////////////////


////////////////////////////
// Control logic for measuring RO 
////////////////////////////

// Define register holding the state
(* keep="true" *) reg [2:0] state = IDLE;
// counter for the clock, referencing to the RO 
reg [31:0] counter_clk = 0;
//reg [31:0] num_measurements_count = 0;

always_ff @(posedge clk_48mhz) begin
    if (reset == 1'b1) begin
        state <= IDLE;
        ro_counter_reset <= 1'b1;
        counter_clk <= 0;
    end else begin
        ro_counter_reset <= 1'b0;

        case (state)
            IDLE: begin
                ro_counter_reset <= 1'b1;
                // if start, 
                if (debug_syncup == 1'b1) begin
                    counter_clk <= 0;
                    state <= ROMEASURE;
                end else begin
                    state <= IDLE;
                end
            end

            ROMEASURE: begin
                if (counter_clk < CLOCKS_PER_BIT) begin
                    counter_clk <= counter_clk + 1'b1;
                    state <= ROMEASURE;
                end else begin
                    ro_comp_count_reg <= ro_counter_count;
                    state <= CONTINUE_MEASURE;
                end
            end

            CONTINUE_MEASURE: begin
                // keep reseting the counter, sleep
                ro_counter_reset <= 1'b1;
                counter_clk <= 0;
                state <= ROMEASURE;
            end

            default: begin
                state <= IDLE;
            end
        endcase
    end
end

endmodule

