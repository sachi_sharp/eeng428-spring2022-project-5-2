`include "ro_defines.sv"
`include "address.sv"

module ro_fomu_top #(
    parameter COUNTER_BIT    = 32,
    parameter RO_TYPE        = `RO_LUT,
    parameter CLOCKS_PER_BIT = 10000,
    parameter NUM_MEAS       = 1000,
	parameter STAGES         = 2
)  (
    input        clki,
    inout        usb_dp, // USB D+ pin
    inout        usb_dn, // USB D- pin
    output       usb_dp_pu
);

// Connect to system clock (with buffering)
wire clkosc;
SB_GB clk_gb (
    .USER_SIGNAL_TO_GLOBAL_BUFFER(clki),
    .GLOBAL_BUFFER_OUTPUT(clkosc)
);

wire clk_48mhz;
assign clk_48mhz = clkosc;

// Generate reset signal
reg [5:0] reset_cnt = 0;
wire reset = ~reset_cnt[5];
always @(posedge clk_48mhz)
    //if ( clk_locked )
        reset_cnt <= reset_cnt + reset;

// uart pipeline in
reg [7:0] uart_in_data;
reg uart_in_valid;
wire uart_in_ready;
wire [7:0] uart_out_data;
wire uart_out_valid;
wire uart_out_ready;

// usb uart - this instanciates the entire USB device.
usb_uart uart (
    .clk_48mhz  (clk_48mhz),
    .reset      (reset),

    // pins
    .pin_usb_p( usb_dp ),
    .pin_usb_n( usb_dn ),

    // uart pipeline in
    .uart_in_data( uart_in_data ),
    .uart_in_valid( uart_in_valid ),
    .uart_in_ready( uart_in_ready ),

    .uart_out_data( uart_out_data ),
    .uart_out_valid( uart_out_valid ),
    .uart_out_ready( uart_out_ready  )

);

// USB Host Detect Pull Up
assign usb_dp_pu = 1'b1;


///////////////////////
// registers and wires
///////////////////////
(* keep="true" *) reg start_count = 0;

////////////////////////////
// local paramters or reg generated from parameters
////////////////////////////

(* keep="true" *) reg         ro_counter_reset = 0;
(* keep="true" *) wire [31:0] ro_counter_count;


////////////////////////////
// State machine parameters
////////////////////////////
localparam IDLE               = 4'd0;
localparam WAIT_RETURN        = 4'd1;
localparam GET_32BITS_DATA    = 4'd2;
localparam RO_COUNTS          = 4'd3;

// state registers
(* keep="true" *) reg [4:0] state = IDLE;
reg [4:0] return_state  = IDLE;

// start the counts for the ro, only high for one cycle
(* keep="true" *) reg  start_count = 0;

// counter to transmit 8 bytes
reg [2:0]   out32counter = 0;
reg [2:0]   in32counter = 0;
// 32bits data to be sent
reg [31:0]  out_32bits_data = 0;

// State machine for looping back UART RX to TX
always_ff @(posedge clk_48mhz) begin
    if (reset == 1'b1) begin
        uart_in_valid <= 0;
        uart_in_data <= 0;
    end
    else begin
        //uart_in_valid <= 0;
        start_count <= 0;

        case (state)
            IDLE: begin
                out32counter <= 0;
                in32counter <= 0;

                if (uart_out_valid && uart_in_ready) begin
                    case (uart_out_data)
                    `GET_RO_COUNTS:          begin start_count <= 1'b1; state <= RO_COUNTS;   end
                    endcase
                end else begin
                    state <= IDLE;
                end
            end

            RO_COUNTS: begin
                out_32bits_data <= ro_comp_count;  
                state <= GET_32BITS_DATA;
            end

            GET_32BITS_DATA: begin
                if (out32counter == 3'b000) begin
                    // values: 000
                    if(uart_in_ready) begin
                        uart_in_valid <= 1'b1;
                        uart_in_data <= out_32bits_data[7:0] ;
                        out_32bits_data <= (out_32bits_data >> 8);
                        out32counter <= out32counter + 1'b1;
                    end 
                    return_state <= GET_32BITS_DATA;
                    state <= WAIT_RETURN;
                end else if (out32counter <= 3'b011) begin
                    // values: 001, 010, 011
                    if(uart_in_ready) begin
                        uart_in_valid <= 1'b1;
                        uart_in_data <= out_32bits_data[7:0] ;
                        out_32bits_data <= (out_32bits_data >> 8);
                        out32counter <= out32counter + 1'b1;
                    end
                    return_state <= GET_32BITS_DATA;
                    state <= WAIT_RETURN;
                end else begin
                    state <= IDLE;
                end
            end

            WAIT_RETURN: begin
                state <= return_state;
            end

            default:
                begin
                    state <= IDLE;  
                end
        endcase
    end
    
end



////////////////////////////
// RO Counter
////////////////////////////
(* keep="true" *) wire [31:0] ro_comp_count;

ro_counter #(
    .COUNTER_BIT (COUNTER_BIT),
    .RO_TYPE(RO_TYPE),
    .CLOCKS_PER_BIT(CLOCKS_PER_BIT),
    .NUM_MEAS(NUM_MEAS),
    .STAGES(STAGES)
) 
ro_counter_INST(
    .clk_48mhz(clk_48mhz), 
    .reset(reset),
    .debug_syncup(start_count),
    .ro_comp_count(ro_comp_count)
);
////////////////////////////
// END
////////////////////////////



endmodule

