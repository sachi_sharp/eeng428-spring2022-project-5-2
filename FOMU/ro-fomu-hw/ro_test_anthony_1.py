#!/usr/bin/python3.8

import time
import serial
import sys
import numpy as np
from matplotlib import pyplot as plt
from addr import *

ro_counts   = []

ser = serial.Serial(
    port="/dev/ttyACM0",
    baudrate=921600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)

time.sleep(0.2)

def read32bitData(ser, addr="TDC_GET_FIFO0_ADDR"):
    ser.write(fsm_addr[addr]) 
    data = 0
    for i in range(4):
        tmp = ser.read()
        data = data + (int.from_bytes(tmp , byteorder=sys.byteorder) << (i * 8))
        # time.sleep(0.2)
    return data


for i in range (10):
        ro = read32bitData(ser, addr = "GET_RO_COUNTS")
        print ("ro counts: ", ro)
        ro_counts.append(ro)

ro_counts = np.array(ro_counts)/10000 *200

fig_ro = plt.hist(ro_counts, bins='auto')
plt.title('RO COUNTS HISTOGRAM')
plt.xlabel("RO Frequency (MHz)")
plt.ylabel("Frequency")
plt.savefig("ro_counts.png")
