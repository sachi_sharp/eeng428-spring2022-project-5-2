module ro_fomu_top (
    output rgb1
);
    wire [6:0] buffers_in, buffers_out;
    assign buffers_in = {buffers_out[5:0], buffers_out[6]};

    SB_LUT4 #(
        .LUT_INIT(16'd1)
    ) buffers [6:0] (
        .O(buffers_out),
        .I0(buffers_in),
        .I1(1'b0),
        .I2(1'b0),
        .I3(1'b0)
    );

    reg [23:0] counter = 0;
    always @(posedge buffers_out[6]) begin
        counter <= counter + 1;
    end

    assign LED = counter[23];

endmodule