
`ifndef RO_DEFINES
`define RO_DEFINES

// typedef enum {
//     RO_LUT,
//     RO_LATCH,
//     RO_REGISTER
// } RO_Type;

`define RO_LUT          1
`define RO_REGISTER     2


`define RO_TYPE      `RO_LUT

`define STAGES      2


`default_nettype wire


`endif
