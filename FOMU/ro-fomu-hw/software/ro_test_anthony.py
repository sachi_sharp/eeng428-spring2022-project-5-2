#!/usr/bin/python3.8

import time
import datetime
import serial
import sys
import numpy as np
from threading import Thread
import threading
import argparse
import subprocess
from matplotlib import pyplot as plt
from addr import *

roCounts   = []
roCounts_str = []
name_str = ""
# roCounts_new    = []
clock_per_bit = 2**22

ser = serial.Serial(
    port="/dev/ttyACM0",
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.EIGHTBITS
)

def timeStamp():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

def saveToFile(data, filename):
    with open (filename, "w") as f:
        for d in data:
            f.write(str(d)+'\n')

def read32bitData(ser, addr="TDC_GET_FIFO0_ADDR"):
    ser.write(fsm_addr[addr]) 
    data = 0
    for i in range(4):
        tmp = ser.read()
        data = data + (int.from_bytes(tmp , byteorder=sys.byteorder) << (i * 8))
        # time.sleep(0.2)
    return data

def null_payload():
    print("Entering null payload thread")

def cpu_stress_payload(time):
    ## stress command
    cmd = ['stress', '--cpu', '1k', '--timeout', time + 's' ]
    subprocess.run(cmd) 

def disk_stress_payload(time):
    ## stress command
    cmd = ['stress', '--hdd', '1k', '--hdd-bytes', '4G', '--timeout', time + 's' ]
    subprocess.run(cmd) 

def network_stress_payload(time):
    cmd = ['speedtest-cli', '--single', '--secure', '--no-pre-allocate', '--timeout', time]
    subprocess.run(cmd) 

# def stress_payload(time):
#     ## stress command
#     cmd = ['stress', '--cpu', '8', '--io', '4', '--vm', '4', '--hdd', '4', '--vm-bytes', '2048M', '--timeout', time + 's' ]
#     subprocess.run(cmd) 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--payload', default=1, type=int, required=False, help='Payload Select.')
    parser.add_argument('-t', '--time', default=60, type=int, required=False, help='Time for data collection.') 
    parser.add_argument('--nofomu', action='store_true', help='FOMU is not used.') 
    args = parser.parse_args()

    payload = args.payload
    seconds = args.time
        
    # reset from software
    ser.write(fsm_addr["RESET_RO"])

    # start ro
    ser.write(fsm_addr["START_RO"])
    # wait for the RO 
    time.sleep(4)

    
    num_reps = 2
    addr = "GET_RO_COUNTS"

    # null payload
    if payload == 1:
        for i in range(num_reps):
            name_str = "null_payload"
            start_time = time.time()
            thread_counter = 0
            while True:
                current_time = time.time()
                elapsed_time = current_time - start_time
                # # write to fifo
                # ser.write(fsm_addr["FIFO_WRITE"])
                ro = read32bitData(ser, addr)
                # print ("ro counts: ", ro)
                roCounts.append(ro)
                roCounts_str.append( timeStamp() + " "+  str(ro))
                time.sleep(0.1)

                if ((elapsed_time > (seconds/2)) and (thread_counter == 0)):
                    thread = Thread(target = null_payload, args = ())
                    thread.start()
                    thread_counter += 1

                if(elapsed_time > seconds):
                    thread.join()

                if(elapsed_time > seconds + seconds/2):
                    break  

    ## CPU Stress   
    if payload == 2:
        for i in range(num_reps):
            name_str = "cpu_stress"
            start_time = time.time()
            thread_counter = 0
            while True:
                current_time = time.time()
                elapsed_time = current_time - start_time
                ro = read32bitData(ser, addr)
                # print ("ro counts: ", ro)
                roCounts.append(ro)
                roCounts_str.append( timeStamp() + " "+  str(ro))
                time.sleep(0.1)

                if ((elapsed_time > (seconds/2)) and (thread_counter == 0)):
                    thread = Thread(target = cpu_stress_payload, args = (str(int(seconds/2)),))
                    thread.start()
                    thread_counter += 1
                
                if(elapsed_time > seconds):
                    thread.join()

                if(elapsed_time > seconds + seconds/2):
                    break  
        
    ## Disk Stress   
    if payload == 3:
        for i in range(num_reps):
            name_str = "disk_stress"
            start_time = time.time()
            thread_counter = 0
            while True:
                current_time = time.time()
                elapsed_time = current_time - start_time
                ro = read32bitData(ser, addr)
                # print ("ro counts: ", ro)
                roCounts.append(ro)
                roCounts_str.append( timeStamp() + " "+  str(ro))
                time.sleep(0.1)

                if ((elapsed_time > (seconds/2)) and (thread_counter == 0)):
                    thread = Thread(target = disk_stress_payload, args = (str(int(seconds/2)),))
                    thread.start()
                    thread_counter += 1

                if(elapsed_time > seconds):
                    thread.join()

                if(elapsed_time > seconds + seconds/2):
                    break  
        
    if payload == 4:
        for i in range(num_reps):
            name_str = "network_stress"
            start_time = time.time()
            thread_counter = 0
            while True:
                current_time = time.time()
                elapsed_time = current_time - start_time
                ro = read32bitData(ser, addr)
                # print ("ro counts: ", ro)
                roCounts.append(ro)
                roCounts_str.append( timeStamp() + " "+  str(ro))
                time.sleep(0.1)

                if ((elapsed_time > (seconds/2)) and (thread_counter == 0)):
                    thread = Thread(target = network_stress_payload, args = (str(int(seconds/2)),))
                    thread.start()
                    thread_counter += 1

                if(elapsed_time > seconds):
                    thread.join()

                if(elapsed_time > seconds + seconds/2):
                    break  

    print("orig",roCounts)
    print("orig",roCounts_str)

    # save data to file 
    dFileName = "tiny_ro_counts_orig_" + name_str + "%s"%(timeStamp())
    saveFile = "%s.npz"%(dFileName)
    print ("Saving to file %s"%(saveFile))
    np.savez_compressed(saveFile, data=np.asarray(roCounts))
    saveToFile(roCounts, "%s.txt"%(dFileName))

    # # save data to file str
    dFileName = "tiny_ro_counts_timeStamp_" + name_str + "%s"%(timeStamp())
    saveFile = "%s.npz"%(dFileName)
    print ("Saving to file %s"%(saveFile))
    np.savez_compressed(saveFile, data=np.asarray(roCounts_str))
    saveToFile(roCounts_str, "%s.txt"%(dFileName))


    # print("orig",roCounts_orig)
    # print("new",roCounts_new)
    # roCounts_orig = (np.array(roCounts_orig)/clock_per_bit) * 48
    # roCounts_orig = np.array(roCounts_orig)/ *48

    fig_ro_1 = plt.hist(roCounts, bins='auto')
    plt.title('FOMU RO COUNTS HISTOGRAM')
    # plt.xlabel("RO Frequency (MHz)")
    plt.xlabel("RO COUNTS")
    plt.ylabel("Frequency")
    plt.savefig("fomu_ro_counts_benchmark_" + name_str + "_histogram.png")
    plt.clf()

    x = list(range(0,len(roCounts))) # num of measurements
    plt.figure(figsize=(25,10))
    fig_ro_2 = plt.plot(x,roCounts, 'ro')
    plt.axvline(x = 300, color = 'b', label = 'axvline - full height')
    plt.axvline(x = 600, color = 'b', label = 'axvline - full height')
    plt.axvline(x = 1200, color = 'b', label = 'axvline - full height')
    plt.axvline(x = 1500, color = 'b', label = 'axvline - full height')
    plt.title('FOMU RO COUNTS')
    # plt.xlabel("RO Frequency (MHz)")
    plt.xlabel("Time (ms)")
    plt.ylabel("RO COUNTS")
    # plt.ylim(3515000, 3524000)
    # plt.ylim(1655975, 1711000) 
    plt.savefig("fomu_ro_counts_benchmark_" + name_str + ".png")
    plt.clf()



