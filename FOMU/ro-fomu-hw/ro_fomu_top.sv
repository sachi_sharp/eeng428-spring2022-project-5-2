`include "ro_defines.sv"
`include "address.sv"

module ro_fomu_top #(
    parameter COUNTER_BIT    = 32,
    parameter RO_TYPE        = `RO_LUT,
    // parameter RO_TYPE        = `RO_REGISTER,
    // parameter CLOCKS_PER_BIT = 1_048_576,
    // parameter CLOCKS_PER_BIT = 129_288,
    // parameter CLOCKS_PER_BIT = 1_000_000,
    // parameter CLOCKS_PER_BIT = 4_194_304,
    parameter CLOCKS_PER_BIT = 8_388_608,
    // parameter CLOCKS_PER_BIT = 2_097_152,
    // parameter FIFO_SAMPLE_DEPTH = 1024,
	parameter STAGES         = 2
)  (
    input        clki,
    inout        usb_dp, // USB D+ pin
    inout        usb_dn, // USB D- pin
    output       usb_dp_pu
);

// Connect to system clock (with buffering)
wire clkosc;
SB_GB clk_gb (
    .USER_SIGNAL_TO_GLOBAL_BUFFER(clki),
    .GLOBAL_BUFFER_OUTPUT(clkosc)
);

wire clk_48mhz;
assign clk_48mhz = clkosc;

// Generate reset signal
reg [5:0] reset_cnt = 0;
wire reset = ~reset_cnt[5];
always @(posedge clk_48mhz)
    //if ( clk_locked )
        reset_cnt <= reset_cnt + reset;

///////////////////////
// rst
///////////////////////
// reg            rst_reg = 0;// active high
// reg [3:0]      rst_init_counter = 0;
// always_ff @(posedge clk_48mhz) begin
//     if (rst_init_counter < 4'd8) begin
//         rst_reg <= 0;
//         rst_init_counter <= rst_init_counter + 1'b1;
//     end else if (rst_init_counter < 4'd12) begin
//         rst_reg <= 1'b1;
//         rst_init_counter <= rst_init_counter + 1'b1;
//     end else begin
//         rst_reg <= 0;
//     end
// end
// wire reset;
// assign reset = rst_reg;

// uart pipeline in
reg [7:0] uart_in_data;
reg uart_in_valid;
wire uart_in_ready;
wire [7:0] uart_out_data;
wire uart_out_valid;
reg uart_out_ready;

// usb uart - this instanciates the entire USB device.
usb_uart uart (
    .clk_48mhz  (clk_48mhz),
    .reset      (reset),

    // pins
    .pin_usb_p( usb_dp ),
    .pin_usb_n( usb_dn ),

    // TX
    .uart_in_data( uart_in_data ),
    .uart_in_valid( uart_in_valid ),
    .uart_in_ready( uart_in_ready ),

    // RX 
    .uart_out_data( uart_out_data ),
    .uart_out_valid( uart_out_valid ),
    .uart_out_ready( uart_out_ready  )

);

// USB Host Detect Pull Up
assign usb_dp_pu = 1'b1;


///////////////////////
// registers and wires
///////////////////////
// start the counts for the ro, only high for one cycle
(* keep="true" *) reg start_count = 0;
// reset signal from Python script, only high for one cycle, active high
(* keep="true" *) reg reset_sw = 0;

////////////////////////////
// RO Counter
////////////////////////////
(* keep="true" *) wire [31:0] ro_comp_count;

(* keep="true" *) 
ro_counter #(
    .COUNTER_BIT (COUNTER_BIT),
    .RO_TYPE(RO_TYPE),
    .CLOCKS_PER_BIT(CLOCKS_PER_BIT),
    .STAGES(STAGES)
) 
ro_counter_INST(
    .clk_48mhz(clk_48mhz), 
    .reset(reset_sw | reset),
    .debug_syncup(start_count),
    .ro_comp_count(ro_comp_count)
);
////////////////////////////
// END
////////////////////////////

////////////////////////////
// Instantiate FIFO
////////////////////////////

// #####################################################
//RX FIFO

// FIFO Parameters
// parameter	BW=32;	// Byte/data width
// parameter 	LGFLEN=10;

// // Write interface
// reg i_wr;
// reg [(BW-1):0]	i_data;
// wire o_full;
// wire [LGFLEN:0]	o_fill;

// Read interface
// reg i_rd;
// wire [(BW-1):0]	o_data;
// wire o_empty;	// True if FIFO is empty
// // reg [(BW-1):0] data_out;
// // #####################################################
// fifo #(
//     .BW(BW),
//     .LGFLEN(LGFLEN)
// ) rx_fifo (
//     .i_clk(clk_48mhz),
//     .i_wr(i_wr),
//     .i_data(i_data),
//     .o_full(o_full),
//     .o_fill(o_fill),
//     .i_rd(i_rd),
//     .o_data(o_data),
//     .o_empty(o_empty)
// );

// WRITE interface
// (* keep="true" *) reg              fifo_wr_en = 0;
// (* keep="true" *) reg [31:0]       fifo_data_wr = 0;
// (* keep="true" *) wire             fifo_full;

// // READ interface
// (* keep="true" *) reg              fifo_rd_en;
// (* keep="true" *) wire [31:0]      fifo_data_rd;
// (* keep="true" *) wire             fifo_empty;

// fifo_mem #(
//     .WIDTH(32),
//     .DEPTH(FIFO_SAMPLE_DEPTH)
// ) fifo_sample_inst (
//     .clk(clk),
//     .rst(rst),
//     .wr(fifo_wr_en),
//     .rd(fifo_rd_en),
//     .din(fifo_data_wr),
//     .empty(fifo_empty),
//     .full(fifo_full),
//     .dout(fifo_data_rd)
// );
////////////////////////////
// END
////////////////////////////

////////////////////////////
// State machine parameters
////////////////////////////
localparam IDLE               = 3'd0;
localparam RO_START           = 3'd1;
localparam WAIT_RETURN        = 3'd2;
localparam GET_32BITS_DATA    = 3'd3;
localparam RO_COUNTS          = 3'd4;
// localparam RO_COUNTS_WAIT    3= 4'd5;
localparam RO_RESET           = 3'd5;
// localparam WRITE_FIFO         = 3'd7;
// localparam WRITE_FIFO_WAIT    = 4'd8;
// localparam DELAY              = 4'd9;

// state registers
(* keep="true" *) reg [2:0] state = IDLE;
reg [2:0] return_state  = IDLE;

// counter to transmit 8 bytes
reg [2:0]   out32counter = 0;
reg [2:0]   in32counter = 0;

// 32bits data to be sent
reg [31:0]  out_32bits_data = 0;
reg [31:0] counter_sleep = 0;

// State machine for looping back UART RX to TX
always @(posedge clk_48mhz) begin
    if (reset == 1'b1) begin
        uart_in_valid <= 0;
        uart_in_data <= 0;
    end
    else begin
        start_count  <= 1'b0;
        reset_sw <= 1'b0;
        // i_wr <= 1'b0;
        // i_rd <= 1'b0;

        case (state)
            IDLE: begin
                out32counter <= 0;
                in32counter <= 0;
                uart_in_data <= 0;
                uart_in_valid <= 1'b0;             

                if (uart_out_valid) begin
                    case (uart_out_data)
                    `START_RO:      begin state <= RO_START;  end
                    `GET_RO_COUNTS: begin state <= RO_COUNTS; end
                    `RESET_RO:      begin state <= RO_RESET;  end   
                    `FIFO_WRITE:    begin state <= WRITE_FIFO; end                 
                    endcase
                    uart_out_ready <= 1'b1;
                end else begin
                    state <= IDLE;
                end
            end

            RO_START: begin
                start_count <= 1'b1; 
                state <= IDLE;
            end

            RO_RESET: begin
                reset_sw <= 1'b1;
                state <= IDLE;
            end

            // WRITE_FIFO: begin
            //     if(o_full == 1'b0) begin
            //         i_wr <= 1'b1;
            //         i_data <= ro_comp_count;
            //         // counter_sleep <= 0;
            //         // state <= WRITE_FIFO_WAIT;
            //         state <= IDLE;
            //     end else begin
            //         state <= IDLE;
            //     end
            // end

            // WRITE_FIFO_WAIT: begin
            //     // i_wr <= 1'b0;
            //     state <= IDLE;
            // end

            // DELAY: begin
            //     if(counter_sleep > 1000000) begin
            //         if(o_full == 1'b1) begin
            //             i_rd <= 1'b1;
            //             // out_32bits_data <= o_data;
            //             // state <= GET_32BITS_DATA; 
            //             state <= IDLE;
            //         end 
            //     end else begin
            //         counter_sleep <= counter_sleep + 1'b1;
            //         state <= DELAY;
            //     end
            // end

            // RO_COUNTS: begin
            //     i_rd <= 1'b1;
            //     // state <= RO_COUNTS_WAIT; 
            //     out_32bits_data <= o_data;
            //     state <= GET_32BITS_DATA; 
            // end

            RO_COUNTS: begin 
                out_32bits_data <= ro_comp_count;
                state <= GET_32BITS_DATA; 
            end

            // RO_COUNTS_WAIT: begin
            //     // i_rd <= 1'b0;
            //     // out_32bits_data <= o_data;
            //     // state <= GET_32BITS_DATA; 
            // end

            GET_32BITS_DATA: begin
                if (out32counter == 3'b000) begin
                    // values: 000
                    if(!uart_in_valid) begin
                        uart_in_valid <= 1'b1;
                        uart_in_data <= out_32bits_data[7:0] ;
                        out_32bits_data <= (out_32bits_data >> 8);
                        out32counter <= out32counter + 1'b1;
                    end 
                    return_state <= GET_32BITS_DATA;
                    state <= WAIT_RETURN;
                end else if (out32counter <= 3'b011) begin
                    // values: 001, 010, 011
                    if(!uart_in_valid) begin
                        uart_in_valid <= 1'b1;
                        uart_in_data <= out_32bits_data[7:0] ;
                        out_32bits_data <= (out_32bits_data >> 8);
                        out32counter <= out32counter + 1'b1;
                    end
                    return_state <= GET_32BITS_DATA;
                    state <= WAIT_RETURN;
                end else begin
                    state <= IDLE;
                end
            end

            WAIT_RETURN: begin
                if(uart_in_ready && uart_in_valid) begin
                    uart_in_valid <= 1'b0;
                    state <= return_state;
                end
            end

            default: 
                begin
                    state <= IDLE;  
                end
        endcase
    end
end

endmodule
