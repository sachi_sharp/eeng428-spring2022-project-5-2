@echo off

set QOMU_BIN=D:\qorc-sdk\qomu_apps

echo testing input program
echo input value COM? from Device Manager

set /P port=enter COM value?
echo comport=%port%

python.exe tinyfpga-programmer-gui.py --mode m4 --port %port% --m4app %QOMU_BIN%\qomu_helloworldsw.bin --reset

pause