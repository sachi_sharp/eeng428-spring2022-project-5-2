module ro_counter #(
    parameter RO_TYPE   = RO_LUT,
    parameter STAGES    = 3 
)(
    input             reset,
    input             enable,
    input             cnt_threshold,    //clock count to stop counting the ro
    output            ro_cnt_vld,       //signals when counting is done
    output [31:0]     ro_cnt            //ro count
);

reg [31:0]   cnt;
reg          ro_cnt_vld_r;
assign ro_cnt_vld = ro_cnt_vld_r;
assign ro_cnt = cnt;

always @ (posedge ro_out)
//reset
if (reset == 1'b1) begin
  cnt <= 0;
//start counting
end else if (enable == 1'b1) begin
  //counting
  cnt <= cnt + 1;
  //if the appropriate clock count is reached
  if (cnt_threshold) begin
    //send to top.v
    cnt <= 0;
  end
end

//ring oscillator
ring_oscillator #(
    .RO_TYPE(RO_TYPE),
    .STAGES(STAGES)
) ro (
    .enable(enable),
    .ro_out(ro_out)
);

endmodule