
`default_nettype wire

typedef enum {
    RO_LUT,
    RO_REGISTER,
    RO_MUX
} RO_Type;

`define RO_TYPE      RO_REGISTER

`define STAGES      3