/*
This is the unfinished top module for the Qomu FPGA ring oscillator.
We did not find a way to transfer the information gathered by the FPGA 
portion to the M4, so there are some unknown elements where the data trasfer
code should go.

*/
module top (
    input wire clk,
    input wire reset
    //might have output
  );

//counting registers
reg [31:0]      count_o;        // keeps track of clock count
reg [31:0]      count_r;        // keeps track ring oscillator count
reg [31:0]      ro_clk_count;   // holds final ring oscillator count 

//resets and enable
reg             clk_rst = 0;
reg             ro_rst = 0;
reg             enable = 0;
//adjustable
reg [31:0]      threshold = 10;


always @ (posedge clk) begin
  //if global reset
if (reset == 1'b1) begin
  ro_clk_count <= 0;
  clk_rst <= 1;
  ro_rst <= 1;
  enable <= 0;

end else begin
  //if inn the middle of counting
  if (count_o > 0) begin
    //if the clock count is at the threshold
    if(count_o == threshold) begin
      ready <= 1;
      ro_clk_count <= count_r;
      //put ro_clk_count somewhere

      //reset counters
      clk_rst <= 1;
      ro_rst <= 1;
      enable <= 0;
    end
    else begin
      ready <= 0;
    end
  //if counters need to be started
  end else begin
    //start the counters for ro and clk
    clk_rst <= 0;
    ro_rst <= 0;
    ready <= 0;
    enable <= 1;
  end
  
end
end

//clock counter
counter counter_c (
    .clk(clk),
    .reset(clk_rst),
    .enable(enable),
    .count(count_o)
);

//ro counter
ro_counter #(
    .RO_TYPE(RO_REGISTER),
    .STAGES(3)
) counter_r (
    .reset(ro_rst),
    .enable(enable),
    .cnt_threshold(ready),
    .ro_cnt(count_r),
    .ro_cnt_vld(valid)
);

endmodule  


