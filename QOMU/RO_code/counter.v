//
// Copyright (c) 2020 QuickLogic Corporation.  All Rights Reserved.
//
// Description :
//    Example of asimple 16(edited to 32) bit up counter in Verilog HDL
//
// Version 1.0 : Initial Creation
//
module counter (clk, reset, enable, count);
input clk, reset, enable;
output [31:0] count;
reg [31:0] count;                                   

always @ (posedge clk)
if (reset == 1'b1) begin
  count <= 0;
end else if (enable == 1'b1) begin
  count <= count + 1;
end

endmodule  
