
/*
 *  
 * Copyright (C) 2019
 * Author: Ilias Giechaskiel <ilias.giechaskiel@gmail.com>
 *          
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
*/


`include "ro_defines.sv"
`include "/home/srs226/Documents/Symbiflow/quicklogic-arch-defs/share/techmaps/quicklogic/pp3/techmap/cells_sim.v"


module ring_oscillator #(
    parameter RO_TYPE   = RO_MUX,
    parameter STAGES    = 3
)(
    input               enable,
    output              ro_out
);

// Added additional stage as otherwise too fast for counting (timing violations)

localparam LAST         = STAGES+1;

(* DONT_TOUCH = "TRUE" *)
wire [LAST:0]           ro_wire;

assign ro_out = ro_wire[LAST];

(* DONT_TOUCH = "TRUE" *)
LUT2 #(
    .INIT               (4'b0100)
) ro_inv (
    .I0                 (ro_wire[0]),
    .I1                 (enable),
    .O                  (ro_wire[1])
);

genvar i;
generate

//STAGES-1 amount of inverters
for (i = 0; i < STAGES; i = i + 1) begin : ro_stages
    (* DONT_TOUCH = "TRUE" *)
    LUT1  #(
        .INIT           (2'b10)
    ) ro_buf (
        .I0             (ro_wire[i + 1]),
        .O              (ro_wire[i + 2])
    );
end

//if look up table RO
if (RO_TYPE == RO_LUT) begin

if (STAGES < 0) begin
assign ro_wire[0] = ro_wire[LAST];
end else begin
(* DONT_TOUCH = "TRUE" *)
LUT1  #(
   .INIT                (2'b10)
) ro_lut (
    .I0                 (ro_wire[LAST]),
    .O                  (ro_wire[0])
);
end

//if register RO
end else if (RO_TYPE == RO_REGISTER) begin

(* DONT_TOUCH = "TRUE" *)
wire ff_clk;

(* DONT_TOUCH = "TRUE" *)
LUT1  #(
   .INIT                (2'b01)
) ro_clk_lut (
    .I0                 (ro_wire[LAST]),
    .O                  (ff_clk)
);

(* DONT_TOUCH = "TRUE" *)
dffp #(
    .INIT               (1'b0)
) ro_ff (
    .PRE                (ro_wire[LAST]),
    .D                  (1'b0),
    .CLK                (ff_clk),
    .Q                  (ro_wire[0])
);

//if multiplexer RO
end else if (RO_TYPE == RO_MUX) begin
    
(* DONT_TOUCH = "TRUE" *)
wire ff_clk;

(* DONT_TOUCH = "TRUE" *)
LUT1  #(
   .INIT                (2'b01)
) ro_clk_lut (
    .I0                 (ro_wire[LAST]),
    .O                  (ff_clk)
);

(* DONT_TOUCH = "TRUE" *)
mux2x0 mux(
  .Q(ro_wire[0]),
  .S(1'b1),
  .A(1'b1),
  .B(ro_wire[LAST])
);

end else begin
initial begin
    $error("Invalid RO_TYPE. Allowed values: RO_LUT (%d), RO_REGISTER (%d), RO_MUX (%d)", RO_LUT, RO_REGISTER, RO_MUX);
end
end

endgenerate

endmodule



