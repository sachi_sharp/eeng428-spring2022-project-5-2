/*==========================================================
 * Copyright 2020 QuickLogic Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *==========================================================*/

/*==========================================================
 *
 *    File   : main.c
 *    Purpose: main for QuickFeather helloworldsw and LED/UserButton test
 *                                                          
 *=========================================================*/

#include "Fw_global_config.h"   // This defines application specific charactersitics

#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"
#include "RtosTask.h"

/*    Include the generic headers required for QORC */
#include "eoss3_hal_gpio.h"
#include "eoss3_hal_rtc.h"
#include "eoss3_hal_fpga_usbserial.h"
#include "ql_time.h"
#include "s3x_clock_hal.h"
#include "s3x_clock.h"
#include "s3x_pi.h"
#include "dbg_uart.h"

#include "cli.h"

#include "fpga_loader.h"    // API for loading FPGA
extern uint32_t axFPGABitStream[];      // FPGA bitstream
extern int      axFPGABitStream_length; // length of axFPGABitStream array in bytes
extern uint32_t axFPGAIOMuxInit[];      // FPGA bitstream
extern int      axFPGAIOMuxInit_length; // length of axFPGABitStream array in bytes

extern const struct cli_cmd_entry my_main_menu[];


const char *SOFTWARE_VERSION_STR;


/*
 * Global variable definition
 */


extern void qomu_hardwaresetup();
static void nvic_init(void);

int main(void)
{

	uint32_t temp;

    SOFTWARE_VERSION_STR = "qorc-sdk/qf_apps/qf_helloworldsw";
    
    qomu_hardwaresetup();
    nvic_init();
    S3x_Clk_Disable(S3X_FB_21_CLK);
    S3x_Clk_Disable(S3X_FB_16_CLK);
    S3x_Clk_Enable(S3X_A1_CLK);
    S3x_Clk_Enable(S3X_CFG_DMA_A1_CLK);
    load_fpga(axFPGABitStream_length,axFPGABitStream);
    fpga_iomux_init(axFPGAIOMuxInit_length,axFPGAIOMuxInit);
    for (volatile int i = 0; i != 4000000; i++) ;   // Give it time to enumerate
    
    // blinking white
    for (volatile int j = 0; j < 8; j++) {
    	(*( volatile unsigned int *) (0x40020008)) = 0xFFFFFFFF;
    	(*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	(*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	for (volatile int i = 0; i != 1000000; i++) ;
    	(*( volatile unsigned int *) (0x40020008)) = 0x0;
    	(*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	(*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	for (volatile int i = 0; i != 1000000; i++) ;
    }

    // blinking red
    for (volatile int j = 0; j < 8; j++) {
        (*( volatile unsigned int *) (0x40020008)) = 0x08000000;
       	(*( volatile unsigned int *) (0x4002000C)) = 0x2;
       	(*( volatile unsigned int *) (0x4002000C)) = 0x0;
       	for (volatile int i = 0; i != 1000000; i++) ;
       	(*( volatile unsigned int *) (0x40020008)) = 0x0;
       	(*( volatile unsigned int *) (0x4002000C)) = 0x2;
       	(*( volatile unsigned int *) (0x4002000C)) = 0x0;
       	for (volatile int i = 0; i != 1000000; i++) ;
        }

    // blinking green
    for (volatile int j = 0; j < 8; j++) {
        (*( volatile unsigned int *) (0x40020008)) = 0x04000000;
        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
        for (volatile int i = 0; i != 1000000; i++) ;
        (*( volatile unsigned int *) (0x40020008)) = 0x0;
        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
        for (volatile int i = 0; i != 1000000; i++) ;
        }

    // blinking blue
    for (volatile int j = 0; j < 8; j++) {
        (*( volatile unsigned int *) (0x40020008)) = 0x02000000;
        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
        for (volatile int i = 0; i != 1000000; i++) ;
        (*( volatile unsigned int *) (0x40020008)) = 0x0;
        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
        for (volatile int i = 0; i != 1000000; i++) ;
        }

    // Rotating 8 colors
    (*( volatile unsigned int *) (0x40020008)) = 0x0;
    (*( volatile unsigned int *) (0x4002000C)) = 0x2;
    (*( volatile unsigned int *) (0x4002000C)) = 0x1;
    for (volatile int i = 0; i != 100000000; i++) ;

    temp = (*( volatile unsigned int *) (0x40020018));

    if (temp && 0x1 < 0x1)
    {
    	// blinking blue
    	    for (volatile int j = 0; j < 8; j++) {
    	        (*( volatile unsigned int *) (0x40020008)) = 0x02000000;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	        for (volatile int i = 0; i != 1000000; i++) ;
    	        (*( volatile unsigned int *) (0x40020008)) = 0x0;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	        for (volatile int i = 0; i != 1000000; i++) ;
    	        }
    }
    else
    {
    	// blinking green
    	    for (volatile int j = 0; j < 8; j++) {
    	        (*( volatile unsigned int *) (0x40020008)) = 0x04000000;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	        for (volatile int i = 0; i != 1000000; i++) ;
    	        (*( volatile unsigned int *) (0x40020008)) = 0x0;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
    	        (*( volatile unsigned int *) (0x4002000C)) = 0x0;
    	        for (volatile int i = 0; i != 1000000; i++) ;
    	        }
    }

    // Rotating 8 colors
        (*( volatile unsigned int *) (0x40020008)) = 0x0;
        (*( volatile unsigned int *) (0x4002000C)) = 0x2;
        (*( volatile unsigned int *) (0x4002000C)) = 0x1;

        
    /* Start the tasks and timer running. */
    vTaskStartScheduler();
    dbg_str("\n");
      
    while(1);
}

static void nvic_init(void)
 {
    // To initialize system, this interrupt should be triggered at main.
    // So, we will set its priority just before calling vTaskStartScheduler(), not the time of enabling each irq.
    NVIC_SetPriority(Ffe0_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
    NVIC_SetPriority(SpiMs_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
    NVIC_SetPriority(CfgDma_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
    NVIC_SetPriority(Uart_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
    NVIC_SetPriority(FbMsg_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
 }    

//needed for startup_EOSS3b.s asm file
void SystemInit(void)
{

}


