`timescale 1ns / 10ps

module cnt32 (
   clock,
   resetN,
   sen,
   cen,
   din,
   count
   );

input clock;
input resetN;
input sen;
input cen;
input [31:0] din;
output [31:0] count;
reg	[31:0]	count;
reg [31:0]	d_count;

always @(*)
begin
	case ({sen, cen})
	2'b00: d_count = count;
	2'b01: d_count = count + 1;
	2'b10: d_count = din;
	2'b11: d_count = din;
	endcase
end

always @(posedge clock or posedge resetN)
begin
	if (resetN)
	begin
		count <= 32'h0;
	end
	else
	begin
		count <= d_count;
	end
end

endmodule
