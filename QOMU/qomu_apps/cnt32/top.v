`timescale 1ns / 10ps
module top ( red, green, blue, t1, t2, t3, t4 );
output red, green, blue, t1, t2, t3, t4;
wire red, green, blue, t1, t2, t3, t4;

wire [31:0] count;

assign red = count[27];
assign green = count[26];
assign blue = count[25];
assign t1 = count[31];
assign t2 = count[30];
assign t3 = count[29];
assign t4 = count[28];

// we need only the clocks/resets from the cell macro
qlal4s3b_cell_macro u_qlal4s3b_cell_macro (
// FB Clocks
.Sys_Clk0       ( Sys_Clk0     ),
.Sys_Clk0_Rst   ( Sys_Clk0_Rst ),
.Sys_Clk1       ( Sys_Clk1     ),
.Sys_Clk1_Rst   ( Sys_Clk1_Rst ),
);

cnt32 dut (
.clock	        ( Sys_Clk0     ),
.resetN         ( Sys_Clk0_Rst ),
.sen            ( 1'b0         ),
.cen            ( 1'b1         ),
.din		    ( 32'h0        ),
.count          ( count        )
);                    

endmodule
